<?xml version="1.0" encoding="utf-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron">
    <ns uri="http://ruslan-strilianyi-strr07.com/semestralni-prace/xml/osobni-doprava" prefix="b"/>
    
    <!--  <value-of select="number(translate(../../../b:datumZalozeni,'-',''))"/> -->
    <pattern>
        <rule context="b:spolecnici">
            <assert test="sum(b:spolecnik/b:podil) = 100">
                Součet podílů spoluvlastníků musí být roven 100.
            </assert> 
        </rule>
    </pattern>
    
    <pattern>
        <rule context="b:spolecnost/b:vozovyPark/b:vozidlo/b:uvedeniDoProvozu">
            <assert test="number(translate(../../../b:datumZalozeni,'-','')) &lt; number(translate(../b:uvedeniDoProvozu,'-',''))">
                Datum uvedení vozidla do provozu musí být pozdější než datum založení společnosti.
            </assert> 
        </rule>
    </pattern>
    
    <pattern>
        <title>Kontrola autobusů</title>
        <rule context="b:spolecnost/b:vozovyPark/b:vozidlo[@typ = 'autobus']">
            <assert test="b:rozmery">
                Autobus musí mít rozměry               
            </assert> 
            <report test="b:pocetVagonu">
                Autobus by neměl mít počet vagonů              
            </report> 
        </rule>
    </pattern>
    
    <pattern>
        <title>Kontrola autobusů</title>
        <rule context="b:spolecnost/b:vozovyPark/b:vozidlo[@typ = 'vlak']">
            <assert test="b:pocetVagonu">
                Vlak musí mít počet vagonů              
            </assert> 
            <report test="b:rozmery">
                Vlak by neměl mít rozměry             
            </report> 
        </rule>
    </pattern>
</schema>