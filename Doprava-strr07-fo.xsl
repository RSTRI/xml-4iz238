<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:o="http://ruslan-strilianyi-strr07.com/semestralni-prace/xml/osobni-doprava"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:output method="xml" encoding="UTF-8"/>
    
    <xsl:template match="/">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="obsahA4" page-height="297mm" page-width="210mm"
                    margin-top="20mm" margin-left="20mm" margin-right="25mm">
                    <fo:region-body margin-bottom="20mm"/>
                    <fo:region-after extent="15mm"/>
                </fo:simple-page-master>
            </fo:layout-master-set>
            
            <fo:page-sequence master-reference="obsahA4">
                <fo:static-content flow-name="xsl-region-after">
                    <fo:block>
                        <fo:page-number/>
                    </fo:block>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body" font-family="Calibri" font-size="16pt">          
                    <xsl:apply-templates/>                    
                </fo:flow>
            </fo:page-sequence>            
        </fo:root>
    </xsl:template>  
    
    <xsl:template match="o:spolecnosti">
        <fo:block font-size="200%" text-align="center" margin-top="20mm">
            Společnosti osobní dopravy na území ČR
        </fo:block>
        
        <fo:block margin-top="20mm">
            <fo:block font-size="200%" text-align="center">Obsah</fo:block>   
            <xsl:for-each select="o:spolecnost">
                          
                <fo:block text-align-last="justify">
                    <fo:basic-link internal-destination="{generate-id()}">
                        <xsl:value-of select="o:nazev"/>
                    </fo:basic-link>
                    <fo:leader leader-pattern="dots"/>
                    <fo:page-number-citation ref-id="{generate-id()}"></fo:page-number-citation>
                </fo:block>
                
            </xsl:for-each>    
        </fo:block>
        
        <xsl:apply-templates select="o:spolecnost"/>
    </xsl:template>   
    
    <xsl:template match="o:spolecnost">
        <fo:block text-align="center" font-size="200%" page-break-before="always" id="{generate-id()}">
            <xsl:value-of select="o:nazev"/>
        </fo:block>
        
        <fo:block margin-left="37%">
            <fo:external-graphic src="url({@src})"
                content-width="5cm"
                width="40%"
                text-align="center"/>
        </fo:block>
        <fo:block color="#bd3035">Základní informace</fo:block>
        <fo:block font-size="80%">Datum založení: <xsl:value-of select="o:datumZalozeni"/></fo:block>
        <fo:block font-size="80%">Sidlo: <xsl:apply-templates select="o:sidlo"/></fo:block>
        <fo:block  margin-top="5mm">
            <xsl:apply-templates select="o:spolecnici"/>
        </fo:block>
        <fo:block margin-top="5mm">
            <fo:block color="#bd3035">Kontaktní osoby</fo:block>
            <fo:block font-size="80%">
                <xsl:apply-templates select="o:kontaktniOsoba"/>  
            </fo:block>                         
        </fo:block>      
        <fo:block margin-top="5mm">
            <xsl:apply-templates select="o:vozovyPark"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="o:spolecnici">
        <fo:block color="#bd3035">Společníci</fo:block>
        <fo:block font-size="80%">
            <xsl:apply-templates select="o:spolecnik"/>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="o:spolecnik">
        <fo:block margin-bottom="3mm">
            <fo:block font-weight="700">
                <xsl:apply-templates select="o:jmeno"/>
                <xsl:text> </xsl:text>
                <xsl:apply-templates select="o:prijmeni"/>
            </fo:block>
            <fo:block>
                Datum narození: <xsl:apply-templates select="o:narozeni"/>
            </fo:block>        
            <fo:block>
                Podil: <xsl:apply-templates select="o:podil"/>
            </fo:block>    
        </fo:block>    
    </xsl:template>
    
    <xsl:template match="o:kontaktniOsoba">
        <fo:block margin-bottom="3mm">
            <fo:block font-weight="700">
                <xsl:apply-templates select="o:jmeno"/>
                <xsl:text> </xsl:text>
                <xsl:apply-templates select="o:prijmeni"/>
            </fo:block>
            <fo:block>
                E-mail: <xsl:apply-templates select="o:email"/>         
            </fo:block>
            <fo:block>
                Telefonní číslo: <xsl:apply-templates select="o:telefonniCislo"/>
            </fo:block>
        </fo:block>        
    </xsl:template>
    
    <xsl:template match="o:vozovyPark">
        <fo:block color="#bd3035">Vozový park</fo:block>
        <fo:block>
            <xsl:for-each-group select="o:vozidlo" group-by="@typ">
                <xsl:sort select="current-grouping-key()" lang="cs"/>
                <fo:block>Typ vozu - <xsl:value-of select="current-grouping-key()"/>:</fo:block>
                <fo:table text-align="center" margin-bottom="3mm">
                    <fo:table-body>
                        <fo:table-row>
                            <xsl:for-each select="o:*">
                                <fo:table-cell>
                                    <fo:block font-size="65%" background-color="grey">
                                        <xsl:apply-templates select="local-name()"/>    
                                    </fo:block>                                    
                                </fo:table-cell>
                            </xsl:for-each>
                        </fo:table-row>
                        
                        <xsl:for-each select="current-group()">
                            <fo:table-row >
                                <xsl:for-each select="o:*">
                                    <fo:table-cell>        
                                        <fo:block font-size="65%">
                                            <xsl:apply-templates select="."/>
                                        </fo:block>
                                    </fo:table-cell>
                                </xsl:for-each>
                            </fo:table-row>
                        </xsl:for-each>
                    </fo:table-body>                   
                </fo:table>
            </xsl:for-each-group>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="o:sidlo">
        <xsl:value-of select="o:mesto"/>
        <xsl:text>, </xsl:text>
        <xsl:value-of select="o:ulice"/>
        <xsl:text>, </xsl:text>
        <xsl:value-of select="o:psc"/>
    </xsl:template>
    
    <xsl:template match="o:rozmery">
        <xsl:apply-templates select="o:delka"/>
        <xsl:text>x</xsl:text>
        <xsl:apply-templates select="o:sirka"/>
        <xsl:text>x</xsl:text>
        <xsl:apply-templates select="o:vyska"/>
    </xsl:template>
    
</xsl:stylesheet>