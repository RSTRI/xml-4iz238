<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
    xmlns:o="http://ruslan-strilianyi-strr07.com/semestralni-prace/xml/osobni-doprava"
    exclude-result-prefixes="o"
    >

    <xsl:output method="html" version="5"/>
    <xsl:output method="html" version="5" name="childPage"/>

    <xsl:template match="/">
         
        <html lang="cs">
            <head>
                <title>
                    <xsl:text>Osobní doprava</xsl:text>
                </title>
                <link rel="stylesheet" href="./styles/mainPage-styles.css"/>
                <link rel="icon" href="https://upload.wikimedia.org/wikipedia/commons/4/44/Icon_S_red.svg" type="image/x-icon"/>
            </head>
            <body>
                <h1>Společnosti osobní dopravy na území ČR</h1>
                <p>Tato stránka obsahuje základní informace o takových společnostech a jejich
                    vozovém parku.</p>
                <div class="container">
                    <xsl:apply-templates select="o:spolecnosti"/>
                </div>
            </body>
        </html>
    </xsl:template>


    <xsl:template match="o:spolecnosti">
        <xsl:apply-templates select="o:spolecnost" mode="mainPage"/>
        <xsl:apply-templates select="o:spolecnost" mode="childPage"/>
    </xsl:template>

    <xsl:template match="o:spolecnost" mode="mainPage">
        <div class="card">
            <h2>
                <xsl:value-of select="o:nazev"/>
            </h2>
            <img>
                <xsl:attribute name="src">
                    <xsl:value-of select="@src"/>
                </xsl:attribute>
                <xsl:attribute name="alt">
                    <xsl:text>obrazovka spolecnosti</xsl:text>
                </xsl:attribute>
            </img>
            <a href="{generate-id(.)}.html">Zjistit více</a>
        </div>
    </xsl:template>

    <xsl:template match="o:spolecnost" mode="childPage">

        <xsl:result-document href="{generate-id()}.html" format="childPage">
            <html lang="cs">
                <head>
                    <title>
                        <xsl:text>Osobní doprava - </xsl:text>
                        <xsl:value-of select="o:nazev"/>
                    </title>
                    <link rel="stylesheet" href="./styles/childPage-styles.css"/>
                    <link rel="icon" href="https://upload.wikimedia.org/wikipedia/commons/4/44/Icon_S_red.svg" type="image/x-icon"/>
                </head>
                <body>
                    <a href="./Doprava-strr07.html" class="button_mainPage">Hlavní strana</a>
                    <h1 class="title">
                        <xsl:value-of select="o:nazev"/>
                    </h1>

                    <div class="container">
                        <h2 class="subtitle">Základní informace</h2>
                        <p>
                            <xsl:text>Datum založení: </xsl:text>
                            <xsl:value-of select="o:datumZalozeni"/>
                        </p>
                        <p>
                            <xsl:text>Sidlo: </xsl:text>
                            <xsl:apply-templates select="o:sidlo"/>
                        </p>
                    </div>

                    <div class="container">
                        <h2 class="subtitle">Společníci</h2>
                        <xsl:apply-templates select="o:spolecnici"/>
                    </div>

                    <div class="container">
                        <h2 class="subtitle">Kontaktní osoby</h2>
                        <xsl:apply-templates select="o:kontaktniOsoba"/>
                    </div>

                    <div class="container">
                        <h2 class="subtitle">Vozový park</h2>
                        <xsl:apply-templates select="o:vozovyPark"/>
                    </div>

                </body>
            </html>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="o:sidlo">
        <xsl:value-of select="o:mesto"/>
        <xsl:text>, </xsl:text>
        <xsl:value-of select="o:ulice"/>
        <xsl:text>, </xsl:text>
        <xsl:value-of select="o:psc"/>
    </xsl:template>

    <xsl:template match="o:spolecnici">
        <xsl:for-each select="o:spolecnik">
            <xsl:sort select="o:podil" data-type="number" order="descending"/>
            <h3>
                <xsl:value-of select="o:jmeno"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="o:prijmeni"/>
            </h3>
            <p>             
                <xsl:text>Datum narození: </xsl:text>
                <xsl:value-of select="o:narozeni"/>
            </p>
            <p>
                <xsl:text>Podil: </xsl:text>
                <xsl:value-of select="o:podil"/>
            </p>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="o:kontaktniOsoba">
        <h3>
            <xsl:value-of select="o:jmeno"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="o:prijmeni"/>
        </h3>
        <p>
            <xsl:text>Telefonní číslo: </xsl:text>
            <xsl:value-of select="o:telefonniCislo"/>
        </p>
        <p>
            <xsl:text>E-mail: </xsl:text>         
            <xsl:value-of select="o:email"/>
        </p>
    </xsl:template>

    <xsl:template match="o:vozovyPark">
        <xsl:for-each-group select="o:vozidlo" group-by="@typ">
            <xsl:sort select="current-grouping-key()" lang="cs"/>
            <h3>Typ vozu - <xsl:value-of select="current-grouping-key()"/>:</h3>
            <table>
                <tr>
                    <xsl:for-each select="o:*">
                        <th>
                            <xsl:apply-templates select="local-name()"/>
                        </th>
                    </xsl:for-each>
                </tr>

                <xsl:for-each select="current-group()">
                    <tr>
                        <xsl:for-each select="o:*">
                            <td>
                                <xsl:choose>

                                    <xsl:when test="name(.) = 'stav'">

                                        <xsl:choose>
                                            <xsl:when test=". = 'oprava'">
                                                <strong class="redText">
                                                  <xsl:value-of select="text()"/>
                                                </strong>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <strong class="greenText">
                                                  <xsl:value-of select="text()"/>
                                                </strong>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:when>
                                    
                                    <xsl:otherwise>
                                        <xsl:apply-templates select="."/>
                                    </xsl:otherwise>
                                </xsl:choose>

                            </td>
                        </xsl:for-each>
                    </tr>
                </xsl:for-each>
            </table>
        </xsl:for-each-group>
    </xsl:template>
    
    <xsl:template match="o:rozmery">
        <xsl:apply-templates select="o:delka"/>
        <xsl:text>x</xsl:text>
        <xsl:apply-templates select="o:sirka"/>
        <xsl:text>x</xsl:text>
        <xsl:apply-templates select="o:vyska"/>
    </xsl:template>
    
</xsl:stylesheet>
